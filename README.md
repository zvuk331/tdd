# Test Driven Development (TDD)

### Links:
#### Как начать думать по TDD: https://ask-dev.ru/info/85832/tdd-how-to-start-really-thinking-tdd
#### Шаги для тестирования: https://coda.io/d/Dom-kz_d7cCZ71QgYt/Test-Driven-Development_suo_v
#### Метрики покрытия тестами: https://coda.io/d/Dom-kz_d7cCZ71QgYt/_su5wh
#### Test containers: https://www.baeldung.com/spring-boot-testcontainers-integration-test
#### Video: https://www.youtube.com/watch?v=9UmwBfZ98U8

#### How To Test Databases Easily in a Spring Boot Application:
https://betterprogramming.pub/how-to-test-databases-easily-in-a-spring-boot-application-5ccdfbc6309f

#### TDD приложений на Spring Boot: работа с базой данных: https://habr.com/ru/post/433958/

#### Есть хорошая книга по TDD: "Экстремальное программирование. Разработка через тестирование"
