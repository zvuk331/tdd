package com.example.tdd.service;

import com.example.tdd.entity.Usr;
import com.example.tdd.repository.UsrRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsrService {

    @Autowired
    UsrRepository usrRepository;
    public Usr save(Usr usr) {
        return usrRepository.save(usr);
    }

    public Usr findById(Long id) {
        return usrRepository.findById(id).orElse(null);
    }
}
