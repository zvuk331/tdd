package com.example.tdd.util;

import javax.swing.text.MaskFormatter;
import java.text.ParseException;

public class UtPhone {
    public static String reformat(String phoneNumber) throws ParseException {
        String mask = "#-###-###-##-##";
        MaskFormatter maskFormatter = null;
        try {
            maskFormatter = new MaskFormatter(mask);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        maskFormatter.setValueContainsLiteralCharacters(false);
        return "+"+ maskFormatter.valueToString(phoneNumber.replaceAll("[^0-9]", "")).replaceFirst("^8", "7");
    }
}
