package com.example.tdd.util;

import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UtPhoneTest {

    // На входе 87762653826, +7-776-265-38-26
    @Test
    public void reformat() throws ParseException {
        String phoneNumber = "87762653826";
        String output = UtPhone.reformat(phoneNumber);

        assertEquals("+7-776-265-38-26", output);
    }

    @Test
    public void reformat2() throws ParseException {
        String phoneNumber = "8(776)2653826";
        String output = UtPhone.reformat(phoneNumber);

        assertEquals("+7-776-265-38-26", output);
    }

    @Test
    public void reformat3() throws ParseException {
        String phoneNumber = "+7-776-265-38-26";
        String output = UtPhone.reformat(phoneNumber);

        assertEquals("+7-776-265-38-26", output);
    }
}
