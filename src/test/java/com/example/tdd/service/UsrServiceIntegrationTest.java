package com.example.tdd.service;

import com.example.tdd.config.TestContainer;
import com.example.tdd.entity.Usr;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class UsrServiceIntegrationTest extends TestContainer {

    @Autowired
    UsrService usrService;

    @Test
    public void create() {
        Usr usr = new Usr("zvuk3331@gmail.com", "8439482894");
        usr = usrService.save(usr);
        assertNotNull(usr.getId());
        assertNotNull(usr.getEmail());
        assertNotNull(usr.getPassword());
    }

    @Test
    @Sql(scripts = {"/test-sql/init.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    public void update() {
        Long id = 5L;
        Usr usr = usrService.findById(id);

        String password = "3287482349";
        usr.setPassword(password);

        usr = usrService.save(usr);

        assertEquals(password, usr.getPassword());
    }


}
